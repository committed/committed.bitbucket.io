webpackJsonp([96084174571474],{

/***/ 503:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", { value: true });
	const React = __webpack_require__(1);
	exports.default = ({ data }) => {
	  const post = data.markdownRemark;
	  return React.createElement("div", null, React.createElement("h1", null, post.frontmatter.title), React.createElement("div", { dangerouslySetInnerHTML: { __html: post.html } }));
	};
	exports.query = "** extracted graphql fragment **";

/***/ })

});
//# sourceMappingURL=component---src-templates-article-tsx-eb19a46f660388e9138b.js.map